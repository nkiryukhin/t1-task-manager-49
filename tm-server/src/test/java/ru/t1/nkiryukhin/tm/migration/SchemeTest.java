package ru.t1.nkiryukhin.tm.migration;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nkiryukhin.tm.marker.DataCategory;
import ru.t1.nkiryukhin.tm.service.ConnectionService;
import ru.t1.nkiryukhin.tm.service.PropertyService;

@Category(DataCategory.class)
public class SchemeTest extends AbstractSchemeTest {

    @Test
    public void test() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);
    }

}
