package ru.t1.nkiryukhin.tm.service;

import liquibase.Liquibase;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;
import ru.t1.nkiryukhin.tm.api.service.IDomainService;

public final class DomainService implements IDomainService {

    @NotNull
    private final IConnectionService connectionService;

    public DomainService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    @SneakyThrows
    public void createDatabase() {
        final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.update("scheme");
    }

    @Override
    @SneakyThrows
    public void dropDatabase() {
        final Liquibase liquibase = connectionService.getLiquibase();
        liquibase.dropAll();
    }

}
