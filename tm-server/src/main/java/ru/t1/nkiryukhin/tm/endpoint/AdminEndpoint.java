package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.api.endpoint.IAdminEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IDomainService;
import ru.t1.nkiryukhin.tm.api.service.IPropertyService;
import ru.t1.nkiryukhin.tm.api.service.IServiceLocator;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseCreateRequest;
import ru.t1.nkiryukhin.tm.dto.request.DatabaseDropRequest;
import ru.t1.nkiryukhin.tm.dto.response.DatabaseCreateResponse;
import ru.t1.nkiryukhin.tm.dto.response.DatabaseDropResponse;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.endpoint.IAdminEndpoint")
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    private IDomainService getDomainService() {
        return this.getServiceLocator().getDomainService();
    }

    private IPropertyService getPropertyService() {
        return this.getServiceLocator().getPropertyService();
    }

    @NotNull
    @Override
    @WebMethod
    public DatabaseDropResponse dropDatabase(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DatabaseDropRequest request
    ) throws AccessDeniedException {
        String passphrase = request.getPassphrase();
        if (passphrase == null || !passphrase.equals(getPropertyService().getDbaPassphrase()))
            throw new AccessDeniedException();
        getDomainService().dropDatabase();
        return new DatabaseDropResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DatabaseCreateResponse createDatabase(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DatabaseCreateRequest request
    ) {
        getDomainService().createDatabase();
        return new DatabaseCreateResponse();
    }

}