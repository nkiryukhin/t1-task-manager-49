package ru.t1.nkiryukhin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.nkiryukhin.tm.api.service.IServiceLocator;
import ru.t1.nkiryukhin.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.nkiryukhin.tm.api.service.dto.ITaskDTOService;
import ru.t1.nkiryukhin.tm.dto.model.SessionDTO;
import ru.t1.nkiryukhin.tm.dto.model.TaskDTO;
import ru.t1.nkiryukhin.tm.dto.request.*;
import ru.t1.nkiryukhin.tm.dto.response.*;
import ru.t1.nkiryukhin.tm.enumerated.Sort;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectTaskDTOService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    private ITaskDTOService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
        return new TaskCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final TaskDTO task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskGetByIdResponse getTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskGetByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = getTaskService().findOneById(userId, id);
        return new TaskGetByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListByProjectIdResponse listTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListByProjectIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @NotNull final List<TaskDTO> tasks = getTaskService().findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskListRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @NotNull final List<TaskDTO> tasks = getTaskService().findAll(userId, sort);
        return new TaskListResponse(tasks);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final TaskDTO task = getTaskService().findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        getTaskService().findOneById(userId, id);
        getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        return new TaskStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) throws AbstractException {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String taskName = request.getName();
        @Nullable final String taskDescription = request.getDescription();
        getTaskService().updateById(userId, id, taskName, taskDescription);
        return new TaskUpdateByIdResponse();
    }

}
