package ru.t1.nkiryukhin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public final class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new YAMLMapper();

    @SneakyThrows
    public void log(@NotNull final String yaml) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(yaml, LinkedHashMap.class);
        @NotNull final String table = event.get("table").toString();
        System.out.println(yaml);
        final byte[] bytes = yaml.getBytes();
        @NotNull final String fileName = table + ".yaml";
        @NotNull final File file = new File(fileName);
        if (!file.exists()) file.createNewFile();
        Files.write(Paths.get(fileName), bytes, StandardOpenOption.APPEND);
    }

}