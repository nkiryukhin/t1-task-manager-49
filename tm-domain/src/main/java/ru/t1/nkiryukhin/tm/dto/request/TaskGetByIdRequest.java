package ru.t1.nkiryukhin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskGetByIdRequest extends AbstractUserRequest {

    @NotNull
    private String id;

    public TaskGetByIdRequest(@Nullable final String token) {
        super(token);
    }

}
