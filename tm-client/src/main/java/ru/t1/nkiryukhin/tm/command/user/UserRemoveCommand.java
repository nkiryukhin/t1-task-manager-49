package ru.t1.nkiryukhin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.nkiryukhin.tm.dto.request.UserRemoveRequest;
import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "user remove";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();

        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().removeUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
