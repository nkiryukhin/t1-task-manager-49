package ru.t1.nkiryukhin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nkiryukhin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.nkiryukhin.tm.api.endpoint.IUserEndpoint;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;
import ru.t1.nkiryukhin.tm.dto.model.UserDTO;
import ru.t1.nkiryukhin.tm.exception.user.UserNotFoundException;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@Nullable final UserDTO user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return serviceLocator.getUserEndpoint();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return serviceLocator.getAuthEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
